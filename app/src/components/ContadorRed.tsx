import { useReducer } from "react";

const initialState = {
    contador: 0,
    
}

type ActionType = 
    | {type: 'incrementar'}
    | {type: 'decrementar'}
    | {type: 'custom', payload: number};

    

//El reducer recibe dos parametros, state y action. y retorna siempre un NUEVO estado gracias a action
const contadorReducer = (state: typeof initialState, action: ActionType) => {
 

    switch (action.type) {

        case 'incrementar':
            return {
                ...state,
                contador: state.contador + 1
            }   
        case 'decrementar':
            return {
                ...state,
                contador: state.contador - 1
            } 
        case 'custom':
            return {
                ...state,
                contador: action.payload
                } 
        default:
            return state;
    }
}

export const ContadorRed = () => {

    //contadorState: viene todo lo que tiene la constante initialState, es decir, viene el estado incial.
    //dispatch es una funcion que se dispara para hacer acciones
    //los prametros de useReducer:
    //me pide la funcion para retornar un nuevo state, en este caso contadorReducer.
    //luego me pide el estado inicial, en este caso initialState
    

    const [ contadorState, dispatch ] = useReducer(contadorReducer, initialState)
  
  
  
  //en dispatch me pide el valor que tiene que ser de tipo ActionType
    return (
    <div>
        <h2>Contador: {contadorState.contador}</h2>
        

        <button 
            className="btn btn-outline-primary"
            onClick={() => dispatch({type: 'incrementar'})}
        >
          -1 
        </button>

        <button 
            className="btn btn-outline-primary"
            onClick={() => dispatch({type: 'decrementar'})}
        >
          -1 
        </button>

        <button 
            className="btn btn-outline-danger"
            onClick={() => dispatch({type: 'custom', payload: 100})}
        >
          100  
        </button>

    </div>
    );
};
