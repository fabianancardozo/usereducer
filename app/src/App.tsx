import './App.css';
import { ContadorRed } from './components/ContadorRed';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <h1>useReducer con React y TypeScript</h1>
        <hr/>
        <ContadorRed/>
      </header>
    </div>
  );
}

export default App;

/* Tanto useState como useReducer tienen el mismo objetivo:
manejar el estado local de nuestros componentes. */